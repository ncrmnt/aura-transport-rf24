#include <aura/aura.h>

int main() {
	slog_init(NULL, 18);

	int ret;
	struct aura_node *n = aura_open("rf24", "0a:0b:0c:0d:0e,ae:ed:dd:00:ab,13,2000");
	struct aura_buffer *retbuf;
	slog(0, SLOG_INFO, "%d", n->status);
	aura_wait_status(n, AURA_STATUS_ONLINE);
	slog(0, SLOG_INFO, "%d", n->status);
	slog(0,SLOG_WARN, "Node online");

	ret = aura_call(n, "relay", &retbuf, 0x0);
	aura_buffer_release(retbuf);

	sleep(1);
	ret = aura_call(n, "relay", &retbuf, 0x1);
	slog(0, SLOG_DEBUG, "call ret %d", ret);
	aura_buffer_release(retbuf);
	sleep(1);


    const struct aura_object *o; 

    aura_enable_sync_events(n, 5);
	int count = 5;
    while (count--) { 
            ret = aura_get_next_event(n, &o, &retbuf);
            slog(0, SLOG_DEBUG, "evt get ret %d", ret);
            aura_hexdump("Out buffer", retbuf->data, retbuf->size);
            aura_buffer_release(retbuf);
    }


	aura_close(n);

	return 0;
}
