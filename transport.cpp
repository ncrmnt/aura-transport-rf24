#define ELPP_DISABLE_DEFAULT_CRASH_HANDLING

extern "C" {
#include <aura/aura.h>
#include <aura/private.h>
#include <aura/packetizer.h>
#include <aura/timer.h>
};

#include <librf24/librf24.hpp>
#include <librf24/easylogging++.hpp>
#include <string>

using namespace librf24;
using namespace std;


INITIALIZE_EASYLOGGINGPP;

enum rf24_transport_state {
        RF24_STATE_SEARCH, /* send out HELLO */
        RF24_STATE_DISCOVER, /* Receive discover packets */
        RF24_STATE_ONLINE, /* Working */
};

struct rf24_userdata {
        enum rf24_transport_state state;
        struct aura_node  *node;
        struct aura_buffer *current_call_buf;
        LibRF24Adaptor    *adap;
        LibRF24IOTransfer *ping;
        LibRF24IOTransfer *hello;
        LibRF24IOTransfer *call;
        LibRF24IOTransfer *response;
        int fails;
};


static bool rf24_can_transfer(LibRF24IOTransfer *tr)
{
        return (tr->status() != TRANSFER_RUNNING && tr->status() != TRANSFER_QUEUED);
}

static void change_state(struct rf24_userdata *ud, enum rf24_transport_state state)
{
        slog(3, SLOG_DEBUG, "Transport state change: %d->%d", ud->state, state);        
        ud->state = state;
}

static void report_live(struct rf24_userdata *ud, bool online)
{
        if (online) {
                ud->fails = 0;
        } else {
                ud->fails++;
        }
        if (ud->fails > 5) {
                change_state(ud, RF24_STATE_SEARCH);
                aura_set_status(ud->node, AURA_STATUS_OFFLINE);
        }
}

enum {
        RF24_AURA_OP_PING = 0,
        RF24_AURA_OP_HELLO,
        RF24_AURA_OP_CALL,
        RF24_AURA_OP_EVENT,
        RF24_AURA_OP_SETTINGS
};

/* PROTOCOL: 

 - Send OP_HELLO with local address and wait (up to 1s) for discovery information & settings packets
 - RF24_AURA_OP_SETTINGS is an optional packet with recommended input-output settings
 - 
 - Send OP_PING periodically to check if the target is alive
 - Send OP_CALL to issue a call
 - Receive call & event data  

*/

#define MAGIC 0x1234


struct __attribute__((packed)) rf24_aura_packet
{
    uint8_t op; /* should include incremental counter */
    uint8_t random; /* randomize */
    union {
            struct {
                uint8_t peer_address[5];
            };
            struct {
                uint8_t id; /* MSB denotes event/call */
                char data[29]; /* serialized object data */
            };
    };
};

void responseTransferDoneCb(LibRF24Transfer &t);
static void submitTransfer(LibRF24IOTransfer &io)
{
        struct rf24_userdata *ud = (struct rf24_userdata *) io.getUserData();
//        if (ud->response->status() == TRANSFER_RUNNING) {
//                /* TODO: Stop receiving packets, process queue */
//        }
        io.submit();
//        ud->response->setTimeout(500);
//        ud->response->setCallback(responseTransferDoneCb);
//        ud->response->submit();
}

void discTransferDoneCb(LibRF24Transfer &t)
{
        LibRF24IOTransfer &io = (LibRF24IOTransfer &)t;
        struct rf24_userdata *ud = (struct rf24_userdata *) io.getUserData();
        slog(4, SLOG_DEBUG, "Got discovery information, %d packets", io.getPacketCount());
        int objects = 0; 

        if (!io.getPacketCount()) {
                change_state(ud, RF24_STATE_SEARCH);
                return;
        }

        for (int i=0; i<io.getPacketCount(); i++) {
                rf24_aura_packet *pck = (rf24_aura_packet *) io.getPacket(i)->c_str();
                if (pck->op != RF24_AURA_OP_HELLO)
                        continue;
                if (pck->id != objects) {
                        slog(1, SLOG_WARN, "Missing packets during discovery(got packet %d, expecting %d) ", pck->id, objects);
                        change_state(ud, RF24_STATE_SEARCH);
                        return;
                }
                
                objects++;
        }

        struct aura_export_table *etbl = aura_etable_create(ud->node, objects);
        if (!etbl) {
                BUG(ud->node, "Failed to create etable");
        }

        for (int i=0; i<io.getPacketCount(); i++) {
                rf24_aura_packet *pck = (rf24_aura_packet *) io.getPacket(i)->c_str();
                if (pck->op == RF24_AURA_OP_HELLO) {
                        char *tmp = pck->data;
                        char *name; 
                        char *callfmt; 
                        char *retfmt;
                        int len; 
                        int maxlen = 32;

                        len = strnlen(tmp, maxlen);
                        name = tmp;
                        tmp = &tmp[len+1];
                        maxlen -= len + 1;

                        len = strnlen(tmp, maxlen);
                        callfmt = (len > 0) ? tmp : NULL;
                        tmp = &tmp[len+1];
                        maxlen -= len + 1;

                        retfmt = tmp;

                        aura_etable_add(etbl, name, callfmt, retfmt);
                }
        }
        /* TODO: Perhaps we should parse settings packet here */
        aura_etable_activate(etbl);
        aura_set_status(ud->node, AURA_STATUS_ONLINE);
        change_state(ud, RF24_STATE_ONLINE);
        io.makeRead(1);
        io.setCallback(responseTransferDoneCb);  
        io.setTimeout(50);
        io.submit();
        io.clear();
}

void responseTransferDoneCb(LibRF24Transfer &t)
{
        LibRF24IOTransfer &io = (LibRF24IOTransfer &)t;
        struct rf24_userdata *ud = (struct rf24_userdata *) io.getUserData();
        slog(4, SLOG_DEBUG, "Got call/event information, %d packets", io.getPacketCount());
        if (!io.getPacketCount()) {
                slog(4, SLOG_WARN, "Ooops, no packets, requeuing");
                t.submit();
                return;
        }

        for (int i=0; i<io.getPacketCount(); i++) {
                rf24_aura_packet *pck = (rf24_aura_packet *) io.getPacket(i)->c_str();
                slog(4, SLOG_DEBUG, "PCK[%d]: id=%d op=%d\n", i, pck->id, pck->op);
                if (ud->current_call_buf && pck->op == RF24_AURA_OP_CALL) {
                        memcpy(ud->current_call_buf->data, pck, sizeof(*pck));
                        aura_node_write(ud->node, ud->current_call_buf);
                        ud->current_call_buf = NULL; /* Complete call */
                }

                if (pck->op == RF24_AURA_OP_EVENT) {
                	struct aura_object *o = aura_etable_find_id(ud->node->tbl, pck->id);
                	if (!o) {
                                slog(0, SLOG_WARN, "Unknown event received, id %d. Dropping", pck->id);
		                continue;
                        }
                        struct aura_buffer *buf = aura_buffer_request(ud->node, 32);
                        if (!buf) {
                                BUG(ud->node, "buffer allocation failed");
                        }
                        buf->object = o;
                        memcpy(buf->data, pck, 32);
                        aura_node_write(ud->node, buf);
                }
        }
        /* Go on listening for new stuff */
        io.clear();
}

void callTransferDoneCb(LibRF24Transfer &t)
{
        LibRF24IOTransfer &io = (LibRF24IOTransfer &)t;
        struct rf24_userdata *ud = (struct rf24_userdata *) io.getUserData();
        if (io.getLastWriteResult()) {
                slog(4, SLOG_DEBUG, "Method call reached destination!");
                if (rf24_can_transfer(ud->response)) {
                        slog(4, SLOG_DEBUG, "Reading resp");
                        ud->response->submit();
                }    
        } else {
                /* TODO: Retry, perhaps? */
                slog(4, SLOG_DEBUG, "Method call didn't reach the destination, marking node offline!");                
                aura_call_fail(ud->node, ud->current_call_buf->object);
                change_state(ud, RF24_STATE_SEARCH);
                aura_set_status(ud->node, AURA_STATUS_OFFLINE);
                aura_buffer_release(ud->current_call_buf);
                ud->current_call_buf = NULL;
        }

}
void helloTransferDoneCb(LibRF24Transfer &t)
{
        LibRF24IOTransfer &io = (LibRF24IOTransfer &)t;
        struct rf24_userdata *ud = (struct rf24_userdata *) io.getUserData();
        report_live(ud, io.getLastWriteResult());
        if (io.getLastWriteResult()) {
                slog(4, SLOG_DEBUG, "Hello packet reached destination! Discovery info incoming!");
                change_state(ud, RF24_STATE_DISCOVER);
                ud->response->setCallback(discTransferDoneCb);
                ud->response->submit();                
                return;
        }
        slog(4, SLOG_DEBUG, "No answer for our hello packet, will try again later");

}


void pingTransferDoneCb(LibRF24Transfer &t)
{
        LibRF24IOTransfer &io = (LibRF24IOTransfer &)t;
        struct rf24_userdata *ud = (struct rf24_userdata *) io.getUserData();
        slog(4, SLOG_DEBUG, "Ping transfer completed: %d", t.status());
        report_live(ud, io.getLastWriteResult());

        if (rf24_can_transfer(ud->response)) {
                slog(4, SLOG_DEBUG, "Reading resp");
                ud->response->submit();
        }    

}

void confTransferDoneCb(LibRF24Transfer &t)
{
        slog(4, SLOG_DEBUG, "Config transfer status: %d", t.status());
}

std::vector<std::string> split(const std::string& text, const std::string& delims)
{
    std::vector<std::string> tokens;
    std::size_t start = text.find_first_not_of(delims), end = 0;

    while((end = text.find_first_of(delims, start)) != std::string::npos)
    {
        tokens.push_back(text.substr(start, end - start));
        start = text.find_first_not_of(delims, end);
    }
    if(start != std::string::npos)
        tokens.push_back(text.substr(start));

    return tokens;
}


static void pollfd_added_cb(int fd, short events, void *user_data)
{
	slog(4, SLOG_DEBUG, "rf24: Adding fd %d evts %x to node", fd, events);
	aura_add_pollfds((struct aura_node *)user_data, fd, events);
}

static void pollfd_removed_cb(int fd, void *user_data)
{
	slog(4, SLOG_DEBUG, "rf24: Removing fd %d from node", fd);
	aura_del_pollfds((struct aura_node *)user_data, fd);
}

static void timer_cb_fn(struct aura_node *node, struct aura_timer *timer, void *arg)
{
    struct rf24_userdata *ud = (struct rf24_userdata *) aura_get_userdata(node);
    slog(4, SLOG_DEBUG, "Periodic!");
    if (ud->state == RF24_STATE_SEARCH) {
         if (rf24_can_transfer(ud->hello)) {
                slog(4, SLOG_DEBUG, "Sending hello packet");
                submitTransfer(*ud->hello);
         }
         return;
    }
   
    if (ud->state == RF24_STATE_ONLINE) {
        slog(4, SLOG_DEBUG, "rf24: Pinging remote (%d/%d)", ud->ping->status(), ud->ping->lastWriteOk);
        if (rf24_can_transfer(ud->ping)) {
                slog(4, SLOG_DEBUG, "Sending ping packet");
                submitTransfer(*ud->ping);
        }
    }

    #if 0
    if (rf24_can_transfer(ud->ping)) {
        ud->ping->submit();
    }
    #endif
}



int rf24_open(struct aura_node *node, const char *opts)
{
        unsigned char remote_addr[5] = { 0xb0, 0x0b, 0x10, 0xad, 0xed };
        unsigned char  local_addr[5] = { 0xb0, 0x0b, 0xc0, 0xde, 0xed };
        int channel = 13;

        slog(1, SLOG_INFO, "Opening rf24 transport");
        struct rf24_userdata *ud = (struct rf24_userdata *)malloc(sizeof(*ud));
        if (!ud) {
                BUG(node, "malloc_failed");
        }
        ud->node = node;
        ud->fails = 0;
        string options(opts);
        vector<string> o = split(options, ",");


        sscanf(o.at(0).c_str(), "%hhx:%hhx:%hhx:%hhx:%hhx",
               &local_addr[0],&local_addr[1],&local_addr[2],&local_addr[3],&local_addr[4]);

        sscanf(o.at(1).c_str(), "%hhx:%hhx:%hhx:%hhx:%hhx",
                &remote_addr[0],&remote_addr[1],&remote_addr[2],&remote_addr[3],&remote_addr[4]);
        channel = atoi(o.at(2).c_str());


        LibRF24Transfer *tmp;

        struct rf24_usb_config conf;
        conf.channel          = channel;
    	conf.rate             = RF24_2MBPS;
    	conf.pa               = RF24_PA_MAX;
    	conf.crclen           = RF24_CRC_16;
    	conf.num_retries      = 3;
    	conf.retry_timeout    = 7;
    	conf.dynamic_payloads = 0;
    	conf.payload_size     = 32;
    	conf.ack_payloads     = 0;
    	conf.pipe_auto_ack    = 0xff;

        change_state(ud, RF24_STATE_SEARCH);
        ud->adap = new LibRF24LibUSBAdaptor();

        ud->ping = new LibRF24IOTransfer(*ud->adap);
        ud->hello = new LibRF24IOTransfer(*ud->adap);
        ud->call = new LibRF24IOTransfer(*ud->adap);
        ud->response = new LibRF24IOTransfer(*ud->adap);

        ud->ping->setUserData(ud);
        ud->call->setUserData(ud);
        ud->response->setUserData(ud);
        ud->hello->setUserData(ud);

        /* Prepare hello packet */
        struct rf24_aura_packet hello = {
                .op = RF24_AURA_OP_HELLO,
                .peer_address = { local_addr[0], local_addr[1], local_addr[2], local_addr[3], local_addr[4]},
                };
        ud->hello->makeWriteStream(true);
        ud->hello->fromBuffer((const char *) &hello, sizeof(hello));
        ud->hello->setCallback(helloTransferDoneCb);

        /* Prepare ping packet */
        struct rf24_aura_packet ping = {
                .op = RF24_AURA_OP_PING,
                };
        ud->ping->makeWriteStream(true);
        ud->ping->fromBuffer((const char *) &ping, sizeof(ping));
        ud->ping->setCallback(pingTransferDoneCb);

        ud->call->setCallback(callTransferDoneCb);

        ud->response->makeRead(255);
        ud->response->setTimeout(300);

        /* Configure adapter */
        tmp = new LibRF24ConfTransfer(*ud->adap, &conf);
        tmp->submit();

        tmp = new LibRF24PipeOpenTransfer(*ud->adap, PIPE_WRITE, remote_addr);
        tmp->setCallback(confTransferDoneCb);
        tmp->submit();

        tmp = new LibRF24PipeOpenTransfer(*ud->adap, PIPE_READ_0, local_addr);
        tmp->setCallback(confTransferDoneCb);
        tmp->submit();

        ud->adap->setFdAddedCb(pollfd_added_cb, node);
        ud->adap->setFdRemovedCb(pollfd_removed_cb, node);

        std::vector<std::pair<int, short>> fds = ud->adap->getPollFds();
        for (auto i=0; i < fds.size(); i++) {
            pollfd_added_cb(fds.at(i).first, fds.at(i).second, node);
        }

        aura_set_userdata(node, ud);
        /* Start pinging the node */
        struct aura_timer *tm;
        struct timeval tv;
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        tm = aura_timer_create(node, timer_cb_fn, ud->adap);
        aura_timer_start(tm, AURA_TIMER_PERIODIC, &tv);
        return 0;
}


void rf24_close(struct aura_node *node)
{
        struct rf24_userdata *ud = (struct rf24_userdata *) aura_get_userdata(node);
        slog(1, SLOG_INFO, "Closing rf24 transport");
}

void rf24_handle_event(struct aura_node *node, enum node_event evt, const struct aura_pollfds *fd)
{
        struct rf24_userdata *ud = (struct rf24_userdata *) aura_get_userdata(node);
        if (evt == NODE_EVENT_HAVE_OUTBOUND) {
                slog(4, SLOG_DEBUG, "Handling outbound stuff");
                if (!rf24_can_transfer(ud->call)) {
                        slog(4, SLOG_DEBUG, "Call transfer currently busy, will try later");
                        return;
                }
                struct aura_buffer *buf = aura_node_read(node);
                if (buf->payload_size > 32) {
                        BUG(node, "Message exceeds 32 bytes");
                }
                struct rf24_aura_packet *pck = (struct rf24_aura_packet *) buf->data;
                pck->random = random() % 255;
                pck->op = RF24_AURA_OP_CALL;
                pck->id = (uint8_t) buf->object->id;
                ud->call->makeWriteStream(true);
                ud->call->fromBuffer((const char *) pck, 32);
                ud->current_call_buf = buf;
                submitTransfer(*ud->call);
        }
        ud->adap->loopOnce();
}


static struct aura_transport rf24 = {
        .name			= "rf24",
        .flags			= 0,
        .buffer_overhead	= 3,
        .buffer_offset		= 3,
        .open			= rf24_open,
        .close			= rf24_close,
        .handle_event		= rf24_handle_event,
};
AURA_TRANSPORT(rf24);
